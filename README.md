## Example of repositry that will not build

This very basic install of next & next-auth will not build via yarn build - there is error for missing dependancy mongodb (or random other database provider).

See the steps below to replicate on a brand new report.

### Package Versions:

- next@10.2.2:
- next-auth@^3.23.3:

### Steps to Replicate Issue

```
yarn create next app
cd [new-app-name]
yarn build # works
yarn add next-auth
yarn build # works
echo 'module.exports = { target: "serverless" }' >next.config.js
yarn build # works
```

Create `pages/api/[...nextauth].js` with contents:

```
import NextAuth from "next-auth";
export default NextAuth({
    providers: [],
    secret: "test-secret",
    session: { jwt: true },
    jwt: {},
    pages: {},
    callbacks: {},
    events: {},
    debug: false,
});
```

```
yarn build # ERRORS
```
