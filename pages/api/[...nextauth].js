import NextAuth from "next-auth";

export default NextAuth({
  providers: [],
  secret: "test-secret",
  session: { jwt: true },
  jwt: {},
  pages: {},
  callbacks: {},
  events: {},
  debug: false,
});
